select_from_path <-function(res,n,p,data,node,type_model,gamma_eBIC,
                            regularization_path_size,type_lars,gene_name){
  if (is.element(type_model, c("BIC","eBIC","AIC")) == TRUE){   # Asymptotic penalties applied in the maximization of the log-likelihooh contrast
    if (type_model == "BIC"){
      pen <- log(n)*res$dimension
    }
    if (type_model == "eBIC"){
      pen <- log(n)*res$dimension + 2*gamma_eBIC*log((choose(p,res$dimension)))
    }
    if (type_model == "AIC"){
      pen <- 2*res$dimension
    }
    criterion <- (-2*as.numeric(res$logLik_main) + pen)[1:floor(n/2)]
    if (length(which(criterion == -Inf)) > 0){
      criterion_bis <- criterion[-which(criterion == -Inf)]
      criterion <- criterion_bis
    }
    index_support <- res$support[which.min(criterion)]
    size_support <- res$dimension[which.min(criterion)]
    number_support <- which.min(criterion)
    if(length(res$beta) > 0){   # final estimator and its number of positive coefficients
      beta <- list()
      beta[[1]] <- vector("numeric", p)
      beta[[1]][as.vector(sapply(names(res$beta[[which.min(criterion)]]), function(x) which(x == gene_name)))] <-
        res$beta[[which.min(criterion)]]
      sign_beta <- sum(beta[[1]] > 0)
    }else{
      beta <- "NA"
      sign_beta <- "NA"
    }
  }
  if (is.element(type_model, c("slope_heuristics","dimension_jump","slope_heuristics_E_L",
                               "dimension_jump_E_L")) == TRUE){    # Non-asymptotic penalties applied in the minimization of the Least-squared contrast
    if (type_model == "slope_heuristics"){
      if (length(c(which(res$logLik == +Inf),which(res$logLik == -Inf)))>0){
        forcap <- cbind(as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))],
                        (as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))]/n),
                        as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))],
                        as.vector(res$LS[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))]))
      }else{
        forcap <- cbind(as.vector(res$dimension), (as.vector(res$dimension)/n),
                        as.vector(res$dimension), as.vector(res$LS))
      }
      ResCapushe <- try(capushe(forcap))
      if(class(ResCapushe)=="try-error"){
        ResCapushe <- try(capushe(forcap, pct = 0.1))
        if (class(ResCapushe)!="try-error"){
          ResCapushe <- capushe(forcap, pct = 0.1)
        }
      }else{
        ResCapushe <- capushe(forcap)
      }
      if (class(ResCapushe)!="try-error"){
        number_support <- which(forcap[,1] == as.numeric(ResCapushe@DDSE@model))
        if (length(number_support) >1){
          number_support <- number_support[which.min(forcap[,4][number_support])]
        }
      }
    } 
    if (type_model == "dimension_jump"){
      if (length(c(which(res$logLik == +Inf),which(res$logLik == -Inf)))>0){
        forcap <- cbind(as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))],
                        (as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))]/n),
                        as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))],
                        as.vector(res$LS[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))]))
      }else{
        forcap <- cbind(as.vector(res$dimension), (as.vector(res$dimension)/n),
                        as.vector(res$dimension), as.vector(res$LS))
      }
      ResCapushe <- try(capushe(forcap))
      if(class(ResCapushe)=="try-error"){
        ResCapushe <- try(capushe(forcap, pct = 0.1))
        if (class(ResCapushe)!="try-error"){
          ResCapushe <- capushe(forcap, pct = 0.1)
        }
      }else{
        ResCapushe <- capushe(forcap)
      }
      if (class(ResCapushe)!="try-error"){
        number_support <- which(forcap[,1] == as.numeric(ResCapushe@Djump@model))
        if (length(number_support) >1){
          number_support <- number_support[which.min(forcap[,4][number_support])]
        }
      }
    } 
    if (type_model == "slope_heuristics_E_L"){
      if (length(c(which(res$logLik == +Inf),which(res$logLik == -Inf)))>0){
        forcap <- cbind(as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))],
                        (as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))]/n)*
                          (log(p/as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))])+2.5),
                        as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))], 
                        as.vector(res$LS)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))])
      }else{
        forcap <- cbind(as.vector(res$dimension),
                        (as.vector(res$dimension)/n)*(log(p/as.vector(res$dimension))+2.5),
                        as.vector(res$dimension), as.vector(res$LS))
      }
      ResCapushe <- try(capushe(forcap))
      if(class(ResCapushe)=="try-error"){
        ResCapushe <- try(capushe(forcap, pct = 0.1))
        if (class(ResCapushe)!="try-error"){
          ResCapushe <- capushe(forcap, pct = 0.1)
        }
      }else{
        ResCapushe <- capushe(forcap)
      }
      if (class(ResCapushe)!="try-error"){
        number_support <- which(forcap[,1] == as.numeric(ResCapushe@DDSE@model))
        if (length(number_support) >1){
          number_support <- number_support[which.min(forcap[,4][number_support])]
        }
      }
    } 
    if (type_model == "dimension_jump_E_L"){
      if (length(c(which(res$logLik == +Inf),which(res$logLik == -Inf)))>0){
        forcap <- cbind(as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))],
                        (as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))]/n)*
                          (log(p/as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))])+2.5),
                        as.vector(res$dimension)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))], 
                        as.vector(res$LS)[-c(which(res$logLik == +Inf),which(res$logLik == -Inf))])
      }else{
        forcap <- cbind(as.vector(res$dimension),
                        (as.vector(res$dimension)/n)*(log(p/as.vector(res$dimension))+2.5),
                        as.vector(res$dimension), as.vector(res$LS))
      }
      ResCapushe <- try(capushe(forcap))
      if(class(ResCapushe)=="try-error"){
        ResCapushe <- try(capushe(forcap, pct = 0.1))
        if (class(ResCapushe)!="try-error"){
          ResCapushe <- capushe(forcap, pct = 0.1)
        }
      }else{
        ResCapushe <- capushe(forcap)
      }
      if (class(ResCapushe)!="try-error"){
        number_support <- which(forcap[,1] == as.numeric(ResCapushe@Djump@model))
        if (length(number_support) >1){
          number_support <- number_support[which.min(forcap[,4][number_support])]
        }
      }
    } 
    if (class(ResCapushe)!="try-error"){  # final estimator and its number of positive coefficients
      if(length(res$beta) > 0){
        beta <- list()
        beta[[1]] <- vector("numeric", p)
        beta[[1]][as.vector(sapply(names(res$beta[[number_support]]), function(x) which(x == gene_name)))] <-
          res$beta[[number_support]]
        index_support <- list()
        index_support[[1]] <- gene_name[which(beta[[1]] != 0)]
        size_support <- length(which(beta[[1]] != 0))
        sign_beta <- sum(beta[[1]] > 0)
      }else{
        beta <- "NA"
        sign_beta <- "NA"
        index_support <- list()
        index_support[[1]] <- res$support[[number_support]]
        size_support <- length(index_support[[1]])
      }
    }else{
      number_support <- "NA"
      beta <- "NA"
      sign_beta <- "NA"
      index_support <- list()
      index_support[[1]] <- list()
      size_support <- 0
    }
  }
  res_selection <- data.frame(number_support = number_support,
                              dimension = size_support, support = I(index_support), 
                              beta = I(beta), sign_beta = I(sign_beta))
}
