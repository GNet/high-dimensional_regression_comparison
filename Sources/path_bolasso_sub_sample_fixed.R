path_bolasso_sub_sample <- function(data, n, p, m, node, alpha_lasso, 
                                size_path_stability, type_lars, delta,
                                alpha_random, type_algo, sample_splitting, gene_name){
  index_sampling <- sort(sample(1:n, n, replace=FALSE))
  sampling <- data[index_sampling,]
  regressor <- sampling[,-which(node == gene_name)]
  regressed <- sampling[,which(node == gene_name)]
  ########################  Creation of the regularization paths ######################## 
  index_resampling <- lapply(1:m, function(x) x <- sort(sample(1:n, n, replace = TRUE)))
  sampling_regressor <- lapply(index_resampling, function(x) sampling <- regressor[x,])
  sampling_regressed <- lapply(index_resampling, function(x) sampling <- regressed[x])
  randomized_lasso <- lapply(sampling_regressor, function(x) t(apply(x,1,function(a) a/ runif(p-1, alpha_random, 1))))   
  if (type_algo == "glm"){
    fit_sampling = lapply(1:m, function(i) glmnet(randomized_lasso[[i]], sampling_regressed[[i]], alpha = alpha_lasso, nlambda = size_path_stability, intercept = FALSE))
    support <- lapply(fit_sampling, function(x) support <- 1*(as.matrix(x$beta)!=0))
    for (j in 1:length(support)){
      rownames(support[[j]]) <- gene_name[-which(node==gene_name)]
    }
  }
  if (type_algo == "lars"){
    fit_sampling = lapply(1:m, function(i) lars(randomized_lasso[[i]], sampling_regressed[[i]], type = c(type_lars), normalize = TRUE, intercept = FALSE))
    support <- lapply(fit_sampling, function(x) support <- t(1*(as.matrix(x$beta)!=0)))
    for (j in 1:length(support)){
      rownames(support[[j]]) <- gene_name[-which(node==gene_name)]
    }
  } 
  if (type_algo == "lars_elastic"){
    fit_sampling = lapply(1:m, function(i) enet(randomized_lasso[[i]], sampling_regressed[[i]],lambda = alpha_lasso, normalize = TRUE, intercept = FALSE))
    support_false <- lapply(fit_sampling, function(x) support <- t(1*(as.matrix(x$beta)!=0)))
    support <- lapply(1:m, function(j) support <-  matrix(rep(0, (p-1)*length(fit_sampling[[j]]$actions)), p-1, length(fit_sampling[[j]]$actions)))
    for (j in 1:length(support)){
      rownames(support[[j]]) <- gene_name[-which(node==gene_name)]
    }
    support <- lapply(1:m, function(i) support[[i]][which(as.numeric(is.element(rownames(support[[i]]),rownames(support_false[[i]]))) == 1),] <- support_false[[i]])
  }
  support_size_lambda <- lapply(1:m, function(i) apply(support[[i]], 1, sum)/dim(support[[i]])[2])
  support_simult <- support_size_lambda
  if(sample_splitting == TRUE){
    index_compl <- lapply(index_resampling, function(x) x <- sort(sample((1:n)[-x], n, replace = TRUE)))
    sampling_regressor_compl <- lapply(index_compl, function(x) sampling <- regressor[x,])
    sampling_regressed_compl <- lapply(index_compl, function(x) sampling <- regressed[x])
    randomized_lasso_compl <- lapply(sampling_regressor_compl, function(x) t(apply(x,1,function(a) a/ runif(p-1, alpha_random, 1)))) 
    if (type_algo == "glm"){
      fit_sampling_compl = lapply(1:m, function(i) glmnet(randomized_lasso_compl[[i]], sampling_regressed_compl[[i]], alpha = alpha_lasso, nlambda = size_path_stability, intercept = FALSE))
      support_compl <- lapply(fit_sampling_compl, function(x) support_compl <- 1*(as.matrix(x$beta)!=0))
    }
    if (type_algo == "lars"){
      fit_sampling_compl = lapply(1:m, function(i) lars(randomized_lasso_compl[[i]], sampling_regressed_compl[[i]], type = c(type_lars), normalize = TRUE, intercept = FALSE))
      support_compl <- lapply(fit_sampling_compl, function(x) support_compl <- t(1*(as.matrix(x$beta)!=0)))
    } 
    if (type_algo == "lars_elastic"){
      fit_sampling_compl = lapply(1:m, function(i) enet(randomized_lasso_compl[[i]], sampling_regressed_compl[[i]],lambda = alpha_lasso, normalize = TRUE, intercept = FALSE))
      support_compl_false <- lapply(fit_sampling_compl, function(x) support_compl <- t(1*(as.matrix(x$beta)!=0)))
      support_compl <- lapply(1:m, function(j) support_compl <-  matrix(rep(0, (p-1)*length(fit_sampling_compl[[j]]$actions)), p-1, length(fit_sampling_compl[[j]]$actions)))
      for (j in 1:length(support_compl)){
        rownames(support_compl[[j]]) <- gene_name[-which(node==gene_name)]
      }
      support_compl <- lapply(1:m, function(i) support_compl[[i]][which(as.numeric(is.element(rownames(support_compl[[i]]),rownames(support_compl_false[[i]]))) == 1),] <- support_compl_false[[i]])
    }
    support_size_lambda_compl <- lapply(1:m, function(i) apply(support_compl[[i]], 1, sum)/dim(support_compl[[i]])[2])
    support_simult <- lapply(1:m, function(i) support_size_lambda[[i]]*support_size_lambda_compl[[i]])
  }
  frequency <- list()
  for (i in 1:length(support_simult)){
    frequency[[i]] <- rep(0,p)
    frequency[[i]][which(is.element(gene_name, names(support_simult[[i]])) == TRUE)] <- as.numeric(support_simult[[i]])
    frequency[[i]] <- frequency[[i]][-which(gene_name == node)]
    names(frequency[[i]]) <- gene_name[-which(gene_name == node)]
  }
  res <- data.frame(frequency_covariable = I(frequency))
}

