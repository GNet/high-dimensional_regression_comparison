select_stability <- function(res, data, node, p, n, delta, gene_name){
  frequency <- matrix(rep(0, (p-1)*length(res$frequency_covariable)), p-1, length(res$frequency_covariable))
  index_support_numeric <- list()
  index_support <- list()
  for (i in 1:length(res$frequency_covariable)){
    frequency[,i] <- res$frequency_covariable[[i]]
  }
  frequency <- (apply(frequency, 1, sum))/length(res$frequency_covariable)
  names(frequency) <- gene_name[-which(gene_name == node)]
  index_support_numeric[[1]] <- as.numeric(which(frequency > delta))
  size_support <- length(index_support_numeric[[1]])
  if (size_support >0){
    index_support[[1]] <- names(which(frequency > delta)+1)
  }else{
    index_support[[1]] <- "NA"
  }
  #######################   Re-estimation of the non-zero coefficients onto each model ########################
  beta <- list()
  beta[[1]] <- vector("numeric",p)
  if (size_support != 0){
    regressor <- data[,-which(node == gene_name)] 
    regressed <- data[,which(node == gene_name)]
    re_estimation = lm(regressed ~ regressor[,index_support_numeric[[1]]]-1)
    beta_new <- as.vector(re_estimation$coefficients)
    index_support_numeric[[1]][which(index_support_numeric[[1]]>=which(gene_name == node))] <- index_support_numeric[[1]][which(index_support_numeric[[1]]>=which(gene_name == node))] +1
    beta[[1]][index_support_numeric[[1]]] <- beta_new
  }
  sign_beta <- sum(beta[[1]] > 0)
  res_selection <- data.frame(dimension = size_support, support = I(index_support),
                              beta = I(beta), signe_beta = sign_beta)
}