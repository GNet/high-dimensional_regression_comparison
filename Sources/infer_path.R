infer_path <- function(data,node,n,p,regularization_path_size,
                       type_algo,alpha_lasso,type_lars,gene_name){
  regressor <- data[,-which(node == gene_name)]
  regressed <- data[,which(node == gene_name)]
  ########################   Creation of the regularization path ######################## 
  if (type_algo == "glm"){
    fit = glmnet(regressor, regressed, alpha = alpha_lasso, nlambda = regularization_path_size, intercept = FALSE)
    support=1*(as.matrix(fit$beta)!=0)
    support_bis=t(unique(t(support)))[,-1]    # collection of unique model
  }
  if(type_algo == "lars"){
    fit <- lars(regressor, regressed, type = c(type_lars), normalize = TRUE, intercept = FALSE)
    support=1*(as.matrix(fit$beta)!=0)
    support_bis=t(unique(support))[,-1]  # collection of unique model
  }
  if(type_algo == "lars_elastic"){
    fit <- enet(regressor,regressed, lambda = alpha_lasso, normalize = TRUE, intercept = FALSE)
    support=1*(as.matrix(fit$beta)!=0)
    support_bis=t(unique(support))[,-1]  # collection of unique model
  }
  #######################   Re-estimation of the non-zero coefficients onto each model ########################
  if(length(support_bis) == ncol(regressor)){   # if there is only one model in the collection
    index.support = names(which(support_bis !=0))
    index.support_numeric <- match(index.support, gene_name)
  }
  if(length(support_bis) != ncol(regressor)){
    index.support=apply(support_bis,2,function(a) names(which(a!=0)))
    index.support_numeric <- lapply(index.support, function(x) match(x, gene_name))
  }
  ## rename the variables since the regressed variable is removed
  for (k in 1:length(index.support_numeric)){
    index.support_numeric[[k]][which(index.support_numeric[[k]]>which(gene_name == node))] <- index.support_numeric[[k]][which(index.support_numeric[[k]]>which(gene_name == node))] -1
  }
  ########################   Model characteristics ######################## 
  re_estimation = lapply(index.support_numeric, function(a) lm(regressed ~ regressor[,a]-1)) # Onto each model, re-estimation of non-zero coefficients
  beta_new <- list()
  beta_new <- sapply(re_estimation, function(a) as.vector(a$coefficients))
  for (i in 1:length(index.support)){
    names(beta_new[[i]]) <- index.support[[i]]
  }
  dimmodel=as.vector(sapply(index.support,length))
  beta_new_numeric <- matrix(0, nrow = length(index.support_numeric), ncol = (p-1))
  for (i in 1:length(index.support_numeric)){
    beta_new_numeric[i,index.support_numeric[[i]]] <- beta_new[[i]]
  }
  for (k in 1:length(index.support_numeric)){
    index.support_numeric[[k]][which(index.support_numeric[[k]]>=which(gene_name == node))] <- index.support_numeric[[k]][which(index.support_numeric[[k]]>=which(gene_name == node))] +1
  }
  LS.support = lapply(1:length(index.support_numeric), function(i) 
    (1/length(regressed))*sum((regressed-regressor %*% (beta_new_numeric[i,]))^2))
  sigma_hat2 = sapply(1:length(index.support_numeric), function(i) (1/length(regressed))*sum((regressed- (regressor %*% (beta_new_numeric[i,])))^2)) 
  # Esimtator of the variance when the mean is supposed to be known (here = 0)
  logLik_bis = lapply(1:length(index.support_numeric), function(i) -(length(regressed))*log(sqrt(2*pi)*sqrt(sigma_hat2[i])) -
                        (1/(2*sigma_hat2[i]))*length(regressed)*LS.support[[i]])
  res <- data.frame(dimension = I(dimmodel), LS = unlist(LS.support),
                    support = I(index.support), beta = I(beta_new),
                    support_numeric = I(index.support_numeric), logLik_main = unlist(logLik_bis))   # characteristics of the models
  res_beta_num <- data.frame(beta_numeric = as.matrix(beta_new_numeric))  # beta estimated
  res_path <- list(val1=res, val2 = res_beta_num)
  return(res_path)
}





