#!/bin/bash
#$-S /bin/bash   
#$ -N ModelSelection          
#$-cwd                 
#$-V                  

j=$1
it=$2
Rscript Run_Simulation_model_selection_procedure_mlm.R  $j $it
