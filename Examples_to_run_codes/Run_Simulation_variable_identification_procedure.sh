#!/bin/bash
#$-S /bin/bash   
#$ -N Run_Simulation             
#$-cwd                 
#$-V                  

j=$1
it=$2
Rscript Run_Simulation_variable_identification_procedure_mlm.R  $j $it
