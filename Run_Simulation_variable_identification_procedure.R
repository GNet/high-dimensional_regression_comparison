## script to launch simulations
## author : perrine lacroix
## date : 2019, 23 May

rm(list=objects())


## From data matrix (cluster, scale-free settings) and the index of the column playing the role of the regressed vector (high or low number of neighbours), 
## this algorithm estimates the unknown parameter of the Gaussian linear regression by three steps:
## (1) The construction of a or several model collection(s) with different penalty functions: Lasso, Ridge and Elastic-Net 
## (2) The final variable selection by criterion: Bolasso and Stability Selection with grid or re/sub-samples first fixed, ESCV criterion, Tigress criterion and the knockoffs methods

#Loading
library(huge)  #popular sparse GGM methods
library(glmnet)
library(lars)
library(elasticnet)
library(HDCI)
library(knockoff)
library(kosel)
library(doMC)
library(devtools)
library(tigress)

## loading of functions source
setwd("~/Sources")
source("path_stability_selection_grid_fixed.R")  
## The input of path_stability_selection_grid_fixed.R is the data matrix and the index of the column playing the role of the regressed vector
## Its output is occurence frequency of each variable
## This is a function is specific to Stability Selection criterion when the grid is first fixed in the procedure
source("path_bolasso_grid_fixed.R") 
## The input of path_bolasso_grid_fixed.R is the data matrix and the index of the column playing the role of the regressed vector
## Its output is occurence frequency of each variable
## This is a function is specific to Bolasso criterion when the grid is first fixed in the procedure
source("path_stability_selection_sub_sample_fixed.R") 
## The input of path_stability_selection_sub_sample_fixed.R is the data matrix and the index of the column playing the role of the regressed vector
## Its output is occurence frequency of each variable
## This is a function is specific to Stability Selection criterion when the sub-samples are first fixed in the procedure
source("path_bolasso_sub_sample_fixed.R")
## The input of path_bolasso_sub_sample_fixed.R is the data matrix and the index of the column playing the role of the regressed vector
## Its output is occurence frequency of each variable
## This is a function is specific to Bolasso criterion when the re-samples are first fixed in the procedure
source("select_stability.R")  
## The input of select_stability.R is the output of path_stability_selection_grid_fixed.R or path_bolasso_grid_fixed.R 
## or path_stability_selection_sub_sample_fixed.R or  path_bolasso_sub_sample_fixed.R
## Its output is the selected model among the available collection for Bolasso or Stability Selection criterion
source("select_ESCV.R")
## The input of select_ESCV.R is the data matrix and the index of the column playing the role of the regressed vector
## Its output is the selected model among the available collection
## This is a function for model selection specific to ESCV criterion
source("select_knockoffs.R")
## The input of select_knockoffs.R is the data matrix and the index of the column playing the role of the regressed vector
## Its output is the selected model among the available collection
## This is a function for model selection specific to knockoffs criterion with knockoff.filter R function
source("select_kosel.R")
## The input of select_kosel.R is the data matrix and the index of the column playing the role of the regressed vector
## Its output is the selected model among the available collection
## This is a function for model selection specific to knockoffs criterion with ko.glm R function
source("select_TIGRESS.R")
## The input of select_TIGRESS.R is the data matrix and the index of the column playing the role of the regressed vector
## Its output is the selected model among the available collection
## This is a function for model selection specific to Tigress criterion
source("evaluate_select_on_true_neigh.R")
## The input of evaluate_select_on_true_neigh.R is the output of select_from_path.R
## Its output is the evaluated metrics: mean squared error, recall, specificity, FDR,...

# simulation parameters
number_observation <- 150  # number of observations
number_nodes <- 200   # number of variables
number_group <- 5    # for cluster settings: number of groups of variables
type_huge <- "scale-free"         # "cluster", "scale-free", "FRANK"
#normalization
mean_data_T.F = TRUE
ecart_data_T.F = TRUE
log_data = FALSE
# choice of the variable playing the role of regressed
regression_chosen = "max_of_neighbour"     # "max_of_neighbour" or "center" or "min_of_neighbour" or "independent"
# criteria parameters
nlambda <- 1000    # size of the lambda grid
algo_select_variable = "glm"         # "glm" or "lars" or "lars_elastic"
type_lasso <- 0.5  ## WARNING
            # glmnet:  1: lasso, 0: ridge. A trade-off: elastic_net
            # elastic_net: 0: lasso, 1: ridge. A trade-off: elastic_net
type_lars <- "lasso"              # for lars algorithm: "lasso", "lar", "forward.stagewise" or "stepwise"
number_fold <- 10     # for ESCV criterion
subsampling_number <- 100    # number of sampling for Stability Selection and Bolasso
size_path_stability <- 100   # size of the regularization_path for Stability Selection and Bolasso
PI_thr <- 0.8  # threshold for the final selected variables subset for Stability Selection and Bolasso
          # between 0.6 and 0.9. Default: usually 0.9 for BOLASSO
threshold_randomized_lasso <- 0.5   # threshold for the randomized lasso function: between 0.2 and 0.8. 
          # 1: no randomization, 0: whole randomization
nfolds <- 10   # for ESCV criterion: has to be larger than 2
nbr_step_LARS <- 50              # for TIGRESS  (default: 5), size of the regularization path
number_prediction <- number_observation      # number of observations used to evaluate the mean squared error
sample_splitting <- TRUE    # TRUE or FALSE. For Bolasso and Stability Selection, if we want to add the complementary datasets in the procedure. 

setwd("~/Datasets")
if (type_huge == "cluster"){
  load("data_cluster.Rdata")
}
if (type_huge == "scale-free"){
  load("data_scale-free.Rdata")
}
if (type_huge == "independent"){
  load("data_independent.Rdata")
}
if (type_huge == "FRANK"){
  load("lists_FRANK_22aug19.Rdata")
  # expression_list : the data
  # network_modified_list : the modified network
  # network_list : the network of links
}

if (algo_select_variable == "glm"){
  result_name_Stab_Select_grid <- paste0(paste("resultat_glm_Stab_Select_grid",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
  result_name_Stab_Select_sub_sample <- paste0(paste("resultat_glm_Stab_Select_sub_sample",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
  result_name_Bolasso_grid <- paste0(paste("resultat_glm_Bolasso_grid",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
  result_name_Bolasso_sub_sample <- paste0(paste("resultat_glm_Bolasso_sub_sample",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
  result_name_Stab_Tigress <- paste0(paste("resultat_glm_Tigress",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
  result_name_Stab_ESCV <- paste0(paste("resultat_glm_ESCV",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
  result_name_Stab_knockoffs <- paste0(paste("resultat_glm_knockoffs",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
  result_name_Stab_kosel_W <- paste0(paste("resultat_glm_kosel_W",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
  result_name_Stab_kosel_error <- paste0(paste("resultat_glm_kosel_error",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"lasso",type_lasso,sep="_"),".RData")
}
if (algo_select_variable == "lars"){
  result_name_Stab_Select_grid <- paste0(paste("resultat_lars_Stab_Select_grid",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
  result_name_Stab_Select_sub_sample <- paste0(paste("resultat_lars_Stab_Select_sub_sample",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
  result_name_Bolasso_grid <- paste0(paste("resultat_lars_Bolasso_grid",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
  result_name_Bolasso_sub_sample <- paste0(paste("resultat_lars_Bolasso_sub_sample",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
  result_name_Stab_Tigress <- paste0(paste("resultat_lars_Stab_Tigress",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
  result_name_Stab_ESCV <- paste0(paste("resultat_lars_Stab_ESCV",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
  result_name_Stab_knockoffs <- paste0(paste("resultat_lars_Stab_knockoffs",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
  result_name_Stab_kosel_W <- paste0(paste("resultat_lars_kosel_W",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
  result_name_Stab_kosel_error <- paste0(paste("resultat_lars_kosel_error",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,sep="_"),".RData")
}
if (algo_select_variable == "lars_elastic"){
  result_name_Stab_Select_grid <- paste0(paste("resultat_lars_Stab_Select_grid",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
  result_name_Stab_Select_sub_sample <- paste0(paste("resultat_lars_Stab_Select_sub_sample",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
  result_name_Bolasso_grid <- paste0(paste("resultat_lars_Bolasso_grid",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
  result_name_Bolasso_sub_sample <- paste0(paste("resultat_lars_Bolasso_sub_sample",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
  result_name_Stab_Tigress <- paste0(paste("resultat_lars_Stab_Tigress",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
  result_name_Stab_ESCV <- paste0(paste("resultat_lars_Stab_ESCV",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
  result_name_Stab_knockoffs <- paste0(paste("resultat_lars_Stab_knockoffs",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
  result_name_Stab_kosel_W <- paste0(paste("resultat_lars_Stab_kosel_W",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
  result_name_Stab_kosel_error <- paste0(paste("resultat_lars_Stab_kosel_error",number_observation,number_nodes,subsampling_number,type_huge,regression_chosen,"threshold_accepte",PI_thr,"randomized",threshold_randomized_lasso,"elasticnet",type_lasso,sep="_"),".RData")
}

nb_it = 40   # number of iterations of the procedure

list_perf_select_stab_selection_grid <- list()
list_stab_select_frequency_grid <- list()
list_time_algo_stab_selection_grid <- list()
list_perf_select_bolasso_grid <- list()
list_bolasso_frequency_grid <- list()
list_time_algo_bolasso_grid <- list()
list_perf_select_stab_selection_sub_sample <- list()
list_stab_select_frequency_sub_sample <- list()
list_time_algo_stab_selection_sub_sample <- list()
list_perf_select_bolasso_sub_sample <- list()
list_bolasso_frequency_sub_sample <- list()
list_time_algo_bolasso_sub_sample <- list()
list_perf_select_ESCV <- list()
list_time_algo_ESCV <- list()
list_support_ESCV <- list()
list_perf_select_knockoffs <- list()
list_time_algo_knockoffs <- list()
list_perf_select_kosel_W <- list()
list_time_algo_kosel_W <- list()
list_perf_select_kosel_error <- list()
list_time_algo_kosel_error <- list()
list_frequency_knockoffs <- list()
list_frequency_kosel_W <- list()
list_frequency_kosel_error <- list()
list_perf_select_TIGRESS <- list()
list_time_algo_TIGRESS <- list()
list_frequency_TIGRESS <- list()

##########################################################################

for(it in 1:nb_it){
  #set.seed(it+1234)
  if (type_huge == "FRANK"){
    data = t(expression_list[[it]])[1:number_observation,]
  }else{
    res_simu <- res_simu_list[[it]]   # the loaded data
    data = res_simu$data[1:number_observation,]
  }
  colnames(data) <- sapply(1:number_nodes, function(i) z <- paste(rep("gene",number_nodes)[i],i))
  rownames(data) <- sapply(1:number_observation, function(i) z <- paste(rep("observation",number_observation)[i],i))
  if (type_huge == "FRANK"){
    true_adja = 1*(as.matrix(network_list[[it]])!=0)
  }else{
    if (type_huge == 'independent'){
      true_adja <- matrix(0, nrow = number_nodes, ncol = number_nodes)   # the true network
    }else{
      true_adja = res_simu$theta   # the true network
    }
  }
  colnames(true_adja) <- sapply(1:number_nodes, function(i) z <- paste(rep("gene",number_nodes)[i],i))
  rownames(true_adja) <- sapply(1:number_nodes, function(i) z <- paste(rep("gene",number_nodes)[i],i))
  if (type_huge == "FRANK"){
    data_test = t(expression_list[[it]])[(number_observation+1):(number_observation+number_prediction),]
  }else{
    data_test = res_simu$data[(number_observation+1):(number_observation+number_prediction),]
  }
  colnames(data_test) <- sapply(1:number_nodes, function(i) z <- paste(rep("gene",number_nodes)[i],i))
  rownames(data_test) <- sapply(1:number_prediction, function(i) z <- paste(rep("observation",number_prediction)[i],i+number_observation))
  ## normalization data
  norm_data <- data
  if (log_data == TRUE){
    norm_data = log2(data)
  }
  norm_data <- scale(norm_data, center = mean_data_T.F, scale = ecart_data_T.F)
  norm_data_test <- data_test
  if (log_data == TRUE){
    norm_data_test = log2(data_test)
  }
  norm_data_test <- scale(norm_data_test, center = mean_data_T.F, scale = ecart_data_T.F)
  ## choice of the regressed variable
  if (type_huge == 'independent'){
    node_chosen = paste("gene",1)
    true_adja[,node_chosen] = c(0,res_simu$theta)
    true_neighbors <- true_adja[,node_chosen]
  }else{
    if (regression_chosen == "max_of_neighbour"){
      node_chosen = colnames(norm_data)[which.max(apply(true_adja,2,sum))][1]
    }
    if (regression_chosen == "min_of_neighbour"){
      node_chosen = colnames(norm_data)[which.min(apply(true_adja,2,sum)[which(apply(true_adja,2,sum) != 0)])][1]
    }
    if (regression_chosen == "center"){
      node_chosen = colnames(norm_data)[1]
    }
    true_neighbors <- colnames(true_adja)[which(true_adja[, node_chosen] !=0)]
  }
  ## Step 1: Construction of the regularization paths.
  time.in_stab_select_grid = Sys.time()
  res_path_stab_select_grid <- path_stability_selection_grid_fixed(data = norm_data, n = number_observation, p = number_nodes,
                                                                      m = subsampling_number, node = node_chosen,
                                                                      alpha_lasso = type_lasso, size_path_stability, type_lars, delta = PI_thr,
                                                                      alpha_random = threshold_randomized_lasso, type_algo = algo_select_variable, 
                                                                      sample_splitting, gene_name = colnames(norm_data))
  time.int1_stab_select_grid = Sys.time()
  time.in_bolasso_grid = Sys.time()
  res_path_bolasso_grid <- path_bolasso_grid_fixed(data = norm_data, n = number_observation, p = number_nodes,
                                                                 m = subsampling_number, node = node_chosen,
                                                                 alpha_lasso = type_lasso, size_path_stability, type_lars, delta = PI_thr,
                                                                 alpha_random = threshold_randomized_lasso, type_algo = algo_select_variable, 
                                                                 sample_splitting, gene_name = colnames(norm_data))
  time.int1_bolasso_grid = Sys.time()
  time.in_stab_select_sub_sample = Sys.time()
  res_path_stab_select_sub_sample <- path_stability_selection_sub_sample(data = norm_data, n = number_observation, p = number_nodes,
                                                                           m = subsampling_number, node = node_chosen,
                                                                           alpha_lasso = type_lasso, size_path_stability, type_lars, delta = PI_thr,
                                                                           alpha_random = threshold_randomized_lasso, type_algo = algo_select_variable, 
                                                                           sample_splitting, gene_name = colnames(norm_data))
  time.int1_stab_select_sub_sample = Sys.time()
  time.in_bolasso_sub_sample = Sys.time()
  res_path_bolasso_sub_sample <- path_bolasso_sub_sample(data = norm_data, n = number_observation, p = number_nodes,
                                                           m = subsampling_number, node = node_chosen,
                                                           alpha_lasso = type_lasso, size_path_stability, type_lars, delta = PI_thr,
                                                           alpha_random = threshold_randomized_lasso, type_algo = algo_select_variable, 
                                                           sample_splitting, gene_name = colnames(norm_data))
  time.int1_bolasso_sub_sample = Sys.time()
  ## Step 2: The model selection
  time.int2_stab_select_grid = Sys.time()
  res_select_stab_select_grid = select_stability(res = res_path_stab_select_grid, data = norm_data,
                                                              node = node_chosen, p = number_nodes, n = number_observation,
                                                              delta = PI_thr, gene_name = colnames(norm_data))
  time.out_stab_select_grid = Sys.time()
  time.algo_stab_select_grid = (time.out_stab_select_grid - time.int2_stab_select_grid)+
    (time.int1_stab_select_grid - time.in_stab_select_grid)
  time.int2_bolasso_grid = Sys.time()
  res_select_bolasso_grid = select_stability(res = res_path_bolasso_grid, data = norm_data,
                                                          node = node_chosen, p = number_nodes, n = number_observation,
                                                          delta = PI_thr, gene_name = colnames(norm_data))
  time.out_bolasso_grid = Sys.time()
  time.algo_bolasso_grid = (time.out_bolasso_grid - time.int2_bolasso_grid)+
    (time.int1_bolasso_grid - time.in_bolasso_grid)
  time.int2_stab_select_sub_sample = Sys.time()
  res_select_stab_select_sub_sample = select_stability(res = res_path_stab_select_sub_sample, data = norm_data,
                                                             node = node_chosen, p = number_nodes, n = number_observation,
                                                             delta = PI_thr, gene_name = colnames(norm_data))
  time.out_stab_select_sub_sample = Sys.time()
  time.algo_stab_select_sub_sample = (time.out_stab_select_sub_sample - time.int2_stab_select_sub_sample)+
    (time.int1_stab_select_sub_sample - time.in_stab_select_sub_sample)
  time.int2_bolasso_sub_sample = Sys.time()
  res_select_bolasso_sub_sample = select_stability(res = res_path_bolasso_sub_sample, data = norm_data,
                                                         node = node_chosen, p = number_nodes, n = number_observation,
                                                         delta = PI_thr, gene_name = colnames(norm_data))
  time.out_bolasso_sub_sample = Sys.time()
  time.algo_bolasso_sub_sample = (time.out_bolasso_sub_sample - time.int2_bolasso_sub_sample)+
    (time.int1_bolasso_sub_sample - time.in_bolasso_sub_sample)
  
  time.in_TIGRESS = Sys.time()
  res_select_TIGRESS = Select_TIGRESS(data = norm_data, gene_names = colnames(norm_data), node_chosen, threshold_randomized_lasso, 
                                      subsampling_number, nbr_step_LARS, PI_thr)
  time.out_TIGRESS = Sys.time()
  time.algo_TIGRESS <- time.out_TIGRESS - time.in_TIGRESS
  if (algo_select_variable == "glm"){
    time.in_ESCV = Sys.time()
    res_select_ESCV = Select_ESCV(data = norm_data, node = node_chosen, gene_name = colnames(norm_data), alpha_lasso = type_lasso, 
                                  size_chemin_regularisation = nlambda, nfolds = nfolds)
    time.out_ESCV = Sys.time()
    time.algo_ESCV <- time.out_ESCV - time.in_ESCV
    time.in_knockoffs = Sys.time()
    res_select_knockoffs = Select_knockoffs(data = norm_data, node = node_chosen, gene_name = colnames(norm_data), alpha_lasso = type_lasso, 
                                            size_chemin_regularisation = nlambda)
    time.out_knockoffs = Sys.time()
    time.algo_knockoffs <- time.out_knockoffs - time.in_knockoffs
    time.in_kosel_W = Sys.time()
    res_select_kosel_W = Select_kosel(data = norm_data, node = node_chosen, gene_name = colnames(norm_data), alpha_lasso = type_lasso, 
                                      size_chemin_regularisation = nlambda, threshold_choice = 'threshold_on_W')
    time.out_kosel_W = Sys.time()
    time.algo_kosel_W <- time.out_kosel_W - time.in_kosel_W
    time.in_kosel_error = Sys.time()
    res_select_kosel_error = Select_kosel(data = norm_data, node = node_chosen, gene_name = colnames(norm_data), alpha_lasso = type_lasso, 
                                          size_chemin_regularisation = nlambda, threshold_choice = 'threshold_on_gap')
    time.out_kosel_error = Sys.time()
    time.algo_kosel_error <- time.out_kosel_error- time.in_kosel_error
  }
  ## Step 3: Evaluation of the procedures
  res_perf_select_stab_select_grid = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                                     adj = true_adja, node = node_chosen,
                                                                     res = res_select_stab_select_grid,
                                                                     gene_name = colnames(norm_data),
                                                                     n_pred = number_prediction, data_test)
  res_perf_select_bolasso_grid = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                                 adj = true_adja, node = node_chosen,
                                                                 res = res_select_bolasso_grid,
                                                                 gene_name = colnames(norm_data),
                                                                 n_pred = number_prediction, data_test)
  res_perf_select_stab_select_sub_sample = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                                    adj = true_adja, node = node_chosen,
                                                                    res = res_select_stab_select_sub_sample,
                                                                    gene_name = colnames(norm_data),
                                                                    n_pred = number_prediction, data_test)
  res_perf_select_bolasso_sub_sample = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                                adj = true_adja, node = node_chosen,
                                                                res = res_select_bolasso_sub_sample,
                                                                gene_name = colnames(norm_data),
                                                                n_pred = number_prediction, data_test)
  res_perf_select_TIGRESS = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                          adj = true_adja, node = node_chosen,
                                                          res = res_select_TIGRESS$val1,
                                                          gene_name = colnames(norm_data),
                                                          n_pred = number_prediction, norm_data_test)
  if (algo_select_variable == "glm"){
    res_perf_select_ESCV = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                         adj = true_adja, node = node_chosen,
                                                         res = res_select_ESCV$val1,
                                                         gene_name = colnames(norm_data),
                                                         n_pred = number_prediction, norm_data_test)
    res_perf_select_knockoffs = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                              adj = true_adja, node = node_chosen,
                                                              res = res_select_knockoffs$val1,
                                                              gene_name = colnames(norm_data),
                                                              n_pred = number_prediction, norm_data_test)
    res_perf_select_kosel_W = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                            adj = true_adja, node = node_chosen,
                                                            res = res_select_kosel_W$val1,
                                                            gene_name = colnames(norm_data),
                                                            n_pred = number_prediction, norm_data_test)
    res_perf_select_kosel_error = evaluate_select_on_true_neigh(p = number_nodes, n = number_observation, data,
                                                                adj = true_adja, node = node_chosen,
                                                                res = res_select_kosel_error$val1,
                                                                gene_name = colnames(norm_data),
                                                                n_pred = number_prediction, norm_data_test)
  }
  ## Storage of obtained results 
  list_perf_select_stab_selection_grid[[it]] <- res_perf_select_stab_select_grid
  list_stab_select_frequency_grid[[it]] <- res_path_stab_select_grid$frequency_covariable
  list_time_algo_stab_selection_grid[[it]] <- time.algo_stab_select_grid
  list_perf_select_bolasso_grid[[it]] <- res_perf_select_bolasso_grid
  list_bolasso_frequency_grid[[it]] <- res_path_bolasso_grid$frequency_covariable
  list_time_algo_bolasso_grid[[it]] <- time.algo_bolasso_grid
  list_perf_select_stab_selection_sub_sample[[it]] <- res_perf_select_stab_select_sub_sample
  list_stab_select_frequency_sub_sample[[it]] <- res_path_stab_select_sub_sample$frequency_covariable
  list_time_algo_stab_selection_sub_sample[[it]] <- time.algo_stab_select_sub_sample
  list_perf_select_bolasso_sub_sample[[it]] <- res_perf_select_bolasso_sub_sample
  list_bolasso_frequency_sub_sample[[it]] <- res_path_bolasso_sub_sample$frequency_covariable
  list_time_algo_bolasso_sub_sample[[it]] <- time.algo_bolasso_sub_sample
  list_perf_select_TIGRESS[[it]] <- res_perf_select_TIGRESS
  list_time_algo_TIGRESS[[it]] <- time.algo_TIGRESS
  list_frequency_TIGRESS[[it]] <- res_select_TIGRESS$val2
  if (algo_select_variable == "glm"){
    list_perf_select_ESCV[[it]] <- res_perf_select_ESCV
    list_time_algo_ESCV[[it]] <- time.algo_ESCV
    list_support_ESCV[[it]] <- res_select_ESCV$val2[[1]]
    list_perf_select_knockoffs[[it]] <- res_perf_select_knockoffs
    list_time_algo_knockoffs[[it]] <- time.algo_knockoffs
    list_perf_select_kosel_W[[it]] <- res_perf_select_kosel_W
    list_time_algo_kosel_W[[it]] <- time.algo_kosel_W
    list_perf_select_kosel_error[[it]] <- res_perf_select_kosel_error
    list_time_algo_kosel_error[[it]] <- time.algo_kosel_error
    list_frequency_knockoffs[[it]] <- res_select_knockoffs$val2
    list_frequency_kosel_W[[it]] <- res_select_kosel_W$val2
    list_frequency_kosel_error[[it]] <- res_select_kosel_error$val2
  }
  print(it)
}


##########################################################################

## Saving the results
resultat_stab_select_grid <- data.frame(time_algo = I(list_time_algo_stab_selection_grid),
                                        stab_select = I(list_perf_select_stab_selection_grid),
                                        frequence_apparition = I(list_stab_select_frequency_grid))
resultat_bolasso_grid <- data.frame(time_algo = I(list_time_algo_bolasso_grid),
                                    stab_select = I(list_perf_select_bolasso_grid),
                                    frequence_apparition = I(list_bolasso_frequency_grid))
resultat_stab_select_sub_sample <- data.frame(time_algo = I(list_time_algo_stab_selection_sub_sample),
                                              stab_select = I(list_perf_select_stab_selection_sub_sample),
                                              frequence_apparition = I(list_stab_select_frequency_sub_sample))
resultat_bolasso_sub_sample <- data.frame(time_algo = I(list_time_algo_bolasso_sub_sample),
                                          stab_select = I(list_perf_select_bolasso_sub_sample),
                                          frequence_apparition = I(list_bolasso_frequency_sub_sample))
resultat_ESCV <- data.frame(time_algo = I(list_time_algo_ESCV),
                                select_ESCV = I(list_perf_select_ESCV),
                                support_ESCV = I(list_support_ESCV))
resultat_knockoffs <- data.frame(time_algo = I(list_time_algo_knockoffs),
                                              select_knockoffs = I(list_perf_select_knockoffs),
                                              frequence_knockoffs = I(list_frequency_knockoffs))
resultat_kosel_W <- data.frame(time_algo = I(list_time_algo_kosel_W),
                                            select_kosel_W = I(list_perf_select_kosel_W),
                                            frequence_kosel_W = I(list_frequency_kosel_W))
resultat_kosel_error <- data.frame(time_algo = I(list_time_algo_kosel_error),
                                                select_kosel_error = I(list_perf_select_kosel_error),
                                                frequence_kosel_error = I(list_frequency_kosel_error))
resultat_TIGRESS <- data.frame(time_algo = I(list_time_algo_TIGRESS),
                                    select_TIGRESS = I(list_perf_select_TIGRESS),
                                    frequence_TIGRESS = I(list_frequency_TIGRESS))

setwd("~/Save")
save(resultat_stab_select_grid, file = result_name)
save(resultat_bolasso_grid, file = result_name) 
save(resultat_stab_select_sub_sample, file = result_name)
save(resultat_bolasso_sub_sample, file = result_name)
save(resultat_ESCV, file = result_name)
save(resultat_knockoffs, file = result_name)
save(resultat_kosel_W, file = result_name)
save(resultat_kosel_error, file = result_name)
save(resultat_TIGRESS, file = result_name)

