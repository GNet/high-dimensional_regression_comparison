
setwd("Results")
### AUC 
### Fig1 
setting="independent"
res=read.table(paste0(setting,"_AUC_150_200_0.8.txt"),h=T)
setting="cluster"
tmp=read.table(paste0(setting,"_AUC_150_200_0.8.txt"),h=T)
res=cbind(res,tmp)
setting="scale-free_max"
tmp=read.table(paste0(setting,"_AUC_150_200_0.8.txt"),h=T)
res=cbind(res,tmp)
setting="scale-free_min"
tmp=read.table(paste0(setting,"_AUC_150_200_0.8.txt"),h=T)
res=cbind(res,tmp)
methods=paste0(colnames(res),rep(c("independent","cluster","scale-free_max","scale-free_min"),each=4))
colnames(res)=methods
jpeg(filename=paste0("../Images/","AUC_150.jpeg"),width=2250,height=2250,units="px",pointsize=36)
boxplot(res,col=rep(c("chartreuse","chartreuse4","cyan","cyan4"),4),xaxt="n")
axis(1,at=seq(2.5,4*4,by=4),labels=c("independent","cluster","scale-free_max","scale-free_min"),las=1)
abline(v=c(4.5,8.5,12.5),lty=3)
dev.off()


## function to generate the figures in appendix
representation=function(setting,metric)
{
  res=read.table(paste0(setting,"_",metric,"_150_200_0.8.txt"),h=T) 
  index=c(grep("ebic",colnames(res)),grep("slope",colnames(res)),grep("jump",colnames(res)),grep("linselect",colnames(res)))
  index.bolasso=grep("_b_",colnames(res))
  index.ss=grep("_ss_",colnames(res))
  index.other=setdiff(1:ncol(res),c(index,index.bolasso,index.ss))
  
  jpeg(filename=paste0("../Images/",metric,"_",setting,"1.jpeg"),width=2250,height=2250,units="px",pointsize=36)
  par(oma=c(5,3,0,0))
  boxplot(res[,index],col=rep(c("bisque2","burlywood4","cornsilk3","darksalmon"),each=4),main="",ylim=c(0,max(1,max(res[,index]))),las=2)
  if(metric=="SUPPORT")
  {
    if (setting=="independent")
    {
      load("../Datasets150/data_independent.RData")
      tmp.ind=mean(sapply(res_simu_list,function(x) sum(x$theta!=0)))
      abline(h=tmp.ind,col="red",lwd=2)
    }
    if (setting=="cluster")
    {
      load("../Datasets150/data_cluster.RData")
      tmp.cluster=mean(sapply(res_simu_list,function(x) sum(x$theta[,1])))
      abline(h=tmp.cluster,col="red",lwd=2)
    }
    if (setting=="scale-free_max")
    {
      load("../Datasets150/data_scale-free.RData")
      tmp.max=mean(sapply(res_simu_list,function(x) max(apply(x$theta,1,sum))))
      abline(h=tmp.max,col="red",lwd=2)
    }
    if (setting=="scale-free_min")
    {
      load("../Datasets150/data_scale-free.RData")
      tmp.min=mean(sapply(res_simu_list,function(x) min(apply(x$theta,1,sum))))
      abline(h=tmp.min,col="red",lwd=2)
    }
  }
  if(metric=="MSE")
    abline(h=1,col="red",lwd=3)
  dev.off()
  jpeg(paste0("../Images/",metric,"_",setting,"2.jpeg"),width=2250,height=2250,pointsize=36)
  par(oma=c(5,3,0,0))
  boxplot(res[,c(index.bolasso,index.ss,index.other)],col=c(rep(c("aquamarine","brown"),each=6),rep(c("cadetblue1","chocolate"),each=2),"darkorchid"),ylim=c(0,max(1,max(res[,c(index.bolasso,index.ss,index.other)],na.rm=TRUE))),las=2)
  abline(h=1,col="red",lwd=3)
  if(metric=="SUPPORT")
  {
    if (setting=="independent")
      abline(h=tmp.ind,col="red",lwd=2)
    if (setting=="cluster")
      abline(h=tmp.cluster,col="red",lwd=2)
    if (setting=="scale-free_max")
      abline(h=tmp.max,col="red",lwd=2)
    if (setting=="scale-free_min")
      abline(h=tmp.min,col="red",lwd=2)
  }
  if(metric=="MSE")
    abline(h=1,col="red",lwd=3)
  dev.off()
}
## function to generate the main figures 
representation.restricted.all=function(metric)
{
  #jpeg(filename=paste0("../Images/",metric,"_150.jpeg"),width=2250,height=2250,units="px",pointsize=36)
 # par(mfrow=c(4,1))
  for (setting in c("independent","cluster","scale-free_max","scale-free_min"))
  {
    res=read.table(paste0(setting,"_",metric,"_150_200_0.8.txt"),h=T) 
    index=grep("lars_enet",colnames(res))
    index1=match(c("lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),colnames(res))
    tmp=res[,c(index,index1)]
    names(tmp)=c("eBIC","slope","jump","Linselect","Bolasso","Stab. Sel.","ESCV","Knockoff","Tigress")
    boxplot(tmp,col=c("bisque2","burlywood4","cornsilk3","darksalmon","aquamarine","brown","cadetblue1","chocolate","darkorchid"),outline=FALSE,main="",ylim=c(0,max(apply(na.omit(tmp),2,quantile,0.75))),las=2,ylab=setting)
    abline(v=c(4.5),lty=3,lwd=3)
    if (metric=="MSE" | metric=="RECALL")
     abline(h=1,col="red",lwd=3)
    if(metric=="FDP")
    {
      points(apply(tmp,2,mean),pch=8,cex=2,lwd=3)
      abline(h=0.1,col="red",lwd=3)
      abline(h=0.05,col="red",lwd=3,lty=3) 
    }
    if(metric=="SUPPORT")
    {
      if (setting=="independent")
        abline(h=12.59,col="red",lwd=3)
      if (setting=="cluster")
        abline(h=11.63,col="red",lwd=3)
      if (setting=="scale-free_max")
        abline(h=31.41,col="red",lwd=3)
      if (setting=="scale-free_min")
        abline(h=1,col="red",lwd=3)   
    }  
  }
   # dev.off()
}

representation.restricted.all.2metrics=function( metric1, metric2)
{
 
  jpeg(filename=paste0("../Images/",metric1,"-",metric2,"_150.jpeg"),width=2250,height=2250,units="px",pointsize=36)
  par(mfrow=c(4,2))
  for (setting in c("independent","cluster","scale-free_max","scale-free_min"))
  {
    res=read.table(paste0(setting,"_",metric1,"_150_200_0.8.txt"),h=T) 
    index=grep("lars_enet",colnames(res))
    index1=match(c("lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),colnames(res))
    tmp1=res[,c(index,index1)]
    names(tmp1)=c("eBIC","slope","jump","Linselect","Bolasso","Stab. Sel.","ESCV","Knockoff","Tigress")
    
    res=read.table(paste0(setting,"_",metric2,"_150_200_0.8.txt"),h=T) 
    index=grep("lars_enet",colnames(res))
    index1=match(c("lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),colnames(res))
    tmp2=res[,c(index,index1)]
    names(tmp2)=c("eBIC","slope","jump","Linselect","Bolasso","Stab. Sel.","ESCV","Knockoff","Tigress")
    
    boxplot(tmp1,col=c("bisque2","burlywood4","cornsilk3","darksalmon","aquamarine","brown","cadetblue1","chocolate","darkorchid"),outline=FALSE,main="",ylim=c(0,max(1,max(apply(na.omit(tmp1),2,quantile,0.75)))),las=2,ylab=setting)
    abline(v=c(4.5),lty=3,lwd=3)
    if (metric1 == "MSE" | metric1 == "RECALL" | metric1 == "SPECIFICITY")
      abline(h=1,col="red",lwd=3)
    if(metric1=="FDP")
    {
      points(apply(tmp1,2,mean),pch=8,cex=2,lwd=3)
      abline(h=0.1,col="red",lwd=3)
      abline(h=0.05,col="red",lwd=3,lty=3) 
    }
    if(metric1=="SUPPORT")
    {
      if (setting=="independent")
        abline(h=12.59,col="red",lwd=3)
      if (setting=="cluster")
        abline(h=11.63,col="red",lwd=3)
      if (setting=="scale-free_max")
        abline(h=31.41,col="red",lwd=3)
      if (setting=="scale-free_min")
        abline(h=1,col="red",lwd=3)   
    }  
    boxplot(tmp2,col=c("bisque2","burlywood4","cornsilk3","darksalmon","aquamarine","brown","cadetblue1","chocolate","darkorchid"),outline=FALSE,main="",ylim=c(0,max(max(apply(na.omit(tmp2),2,quantile,0.75)),1)),las=2,ylab=setting)
    abline(v=c(4.5),lty=3,lwd=3)
    if (metric2=="MSE" | metric2=="RECALL" | metric2 == "SPECIFICITY")
      abline(h=1,col="red",lwd=3)
    if(metric2=="FDP")
    {
      points(apply(tmp2,2,mean),pch=8,cex=2,lwd=3)
      abline(h=0.1,col="red",lwd=3)
      abline(h=0.05,col="red",lwd=3,lty=3) 
    }
    if(metric2=="SUPPORT")
    {
      if (setting=="independent")
        abline(h=12.59,col="red",lwd=3)
      if (setting=="cluster")
        abline(h=11.63,col="red",lwd=3)
      if (setting=="scale-free_max")
        abline(h=31.41,col="red",lwd=3)
      if (setting=="scale-free_min")
        abline(h=1,col="red",lwd=3)   
    }  
  }
   dev.off()
}

## function to generate the figures for a given setting
representation.restricted=function(setting,metric)
{
  res=read.table(paste0(setting,"_",metric,"_150_200_0.8.txt"),h=T) 
  jpeg(filename=paste0("../Images/",metric,"_",setting,"_150.jpeg"),width=2250,height=2250,units="px",pointsize=36)
  index=grep("lars_enet",colnames(res))
  index1=match(c("lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),colnames(res))
  tmp=res[,c(index,index1)]
  names(tmp)=c("eBIC","slope","jump","Linselect","Bolasso","Stab. Sel.","ESCV","Knockoff","Tigress")
  boxplot(tmp,col=c("bisque2","burlywood4","cornsilk3","darksalmon","aquamarine","brown","cadetblue1","chocolate","darkorchid"),outline=FALSE,main="",ylim=c(0,max(tmp)),las=2)
  abline(v=c(4.5),lty=3,lwd=3)
  if (metric=="MSE")
    abline(h=1,col="red",lwd=3)
  if(metric=="FDP")
  {
    points(apply(tmp,2,mean),pch=8,cex=2,lwd=2)
    abline(h=0.1,col="red",lwd=3)
    abline(h=0.05,col="red",lwd=3,lty=3) 
  }
  dev.off()
}


### Creation of the figures
setwd("Results")
representation("independent","MSE")
representation("cluster","MSE")
representation("scale-free_max","MSE")
representation("scale-free_min","MSE")

representation.restricted.all("MSE")
representation.restricted.all("RECALL")
representation.restricted.all("FDP")
representation.restricted.all("SUPPORT")
representation.restricted.all("RECALL")
representation.restricted.all("SPECIFICITY")


representation.restricted.all.2metrics("RECALL","SPECIFICITY")
representation.restricted.all.2metrics("SUPPORT","MSE")


representation("independent","RECALL")
representation("cluster","RECALL")
representation("scale-free_max","RECALL")
representation("scale-free_min","RECALL")

representation.restricted("independent","RECALL")
representation.restricted("cluster","RECALL")
representation.restricted("scale-free_max","RECALL")
representation.restricted("scale-free_min","RECALL")

representation("independent","SPECIFICITY")
representation("cluster","SPECIFICITY")
representation("scale-free_max","SPECIFICITY")
representation("scale-free_min","SPECIFICITY")

representation.restricted("independent","SPECIFICITY")
representation.restricted("cluster","SPECIFICITY")
representation.restricted("scale-free_max","SPECIFICITY")
representation.restricted("scale-free_min","SPECIFICITY")

representation("independent","FDP")
representation("cluster","FDP")
representation("scale-free_max","FDP")
representation("scale-free_min","FDP")

representation.restricted("independent","FDP")
representation.restricted("cluster","FDP")
representation.restricted("scale-free_max","FDP")
representation.restricted("scale-free_min","FDP")


representation("independent","SUPPORT")
representation("cluster","SUPPORT")
representation("scale-free_max","SUPPORT")
representation("scale-free_min","SUPPORT")



######################### hierarchical clustering to confirm the groups defined in the discussion
n=150
setting="independent"
metric="RECALL"
res=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="FDP"
res1=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="MSE"
res2=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="SUPPORT"
res3=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
tmp=cbind(support=apply(res3,2,mean),recall=apply(res,2,mean),fdr=apply(res1,2,function(x) mean(x[which(x!=0)],na.rm=TRUE)),mse=apply(res2,2,mean))
index=grep("lars_enet",rownames(tmp))
index1=match(c("lars_lasso_ebic","lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),rownames(tmp))
plot(hclust(dist(tmp[c(index[-1],index1),]),method = "ward.D2"))

setting="cluster"
metric="RECALL"
res=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="FDP"
res1=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="MSE"
res2=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="SUPPORT"
res3=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
tmp=cbind(support=apply(res3,2,mean),recall=apply(res,2,mean),fdr=apply(res1,2,function(x) mean(x[which(x!=0)],na.rm=TRUE)),mse=apply(res2,2,mean))
index=grep("lars_enet",rownames(tmp))
index1=match(c("lars_lasso_ebic","lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),rownames(tmp))
plot(hclust(dist(tmp[c(index[-1],index1),]),method = "ward.D2"))

setting="scale-free_min"
metric="RECALL"
res=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="FDP"
res1=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="MSE"
res2=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="SUPPORT"
res3=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
tmp=cbind(support=apply(res3,2,mean),recall=apply(res,2,mean),fdr=apply(res1,2,function(x) mean(x[which(x!=0)],na.rm=TRUE)),mse=apply(res2,2,mean))
index=grep("lars_enet",rownames(tmp))
index1=match(c("lars_lasso_ebic","lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),rownames(tmp))
plot(hclust(dist(tmp[c(index[-1],index1),]),method = "ward.D2"))

setting="scale-free_max"
metric="RECALL"
res=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="FDP"
res1=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="MSE"
res2=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 

tmp=cbind(recall=apply(res,2,mean),fdr=apply(res1,2,function(x) mean(x[which(x!=0)],na.rm=TRUE)),mse=apply(res2,2,mean))
index=grep("lars_enet",rownames(tmp))
index1=match(c("lars_lasso_ebic","lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),rownames(tmp))
plot(hclust(dist(tmp[c(index[-1],index1),]),method = "ward.D2"))
tmp.max=tmp[c(index[-1],index1),]
rownames(tmp.max)=paste(rownames(tmp.max),"max",sep="-")
setting="scale-free_min"
metric="RECALL"
res=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="FDP"
res1=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="MSE"
res2=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
metric="SUPPORT"
res3=read.table(paste0(setting,"_",metric,"_",n,"_200_0.8.txt"),h=T) 
tmp=cbind(recall=apply(res,2,mean),fdr=apply(res1,2,function(x) mean(x[which(x!=0)],na.rm=TRUE)),mse=apply(res2,2,mean))
index=grep("lars_enet",rownames(tmp))
index1=match(c("lars_lasso_ebic","lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),rownames(tmp))
plot(hclust(dist(rbind(tmp[c(index[-1],index1),],tmp.max)),method = "ward.D2"))

