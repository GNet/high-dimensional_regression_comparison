## to find the average number of active variables
# setwd("Save_model_selection_FRANK")
# tutu=list.files(pattern="result_lars_150_200_FRANK_max_of_neighbour_elasticnet_0.5")
# res = numeric()
# for (i in 1:40)
#   {
#     load(tutu[i])
#     res[i]=result$truth$number_true_neighbour
#    }
# mean(res)
# 

setwd("Results")
### AUC 
### Fig1 
setting="FRANK_max"
res=read.table(paste0(setting,"_AUC_150_200_0.8.txt"),h=T)
setting="FRANK_max_shrinkage"
tmp=read.table(paste0(setting,"_AUC_150_200_0.8.txt"),h=T)
res=cbind(res,tmp)
setting="FRANK_min"
tmp=read.table(paste0(setting,"_AUC_150_200_0.8.txt"),h=T)
res=cbind(res,tmp)
setting="FRANK_min_shrinkage"
tmp=read.table(paste0(setting,"_AUC_150_200_0.8.txt"),h=T)
res=cbind(res,tmp)
methods=paste0(colnames(res),rep(c("max","max_shrinkage","min","min_shrinkage"),each=4))
colnames(res)=methods
jpeg(filename="../Images/AUC_FRANK.jpeg",width=2250,height=2250,units="px",pointsize=36)
boxplot(res,col=rep(c("chartreuse","chartreuse4","cyan","cyan4"),4),xaxt="n")
axis(1,at=seq(2.5,4*4,by=4),labels=c("max","max_shrinkage","min","min_shrinkage"),las=1)
abline(v=c(4.5,8.5,12.5),lty=3)
dev.off()

## function to generate figures as in appendix 
representation=function(setting,metric)
{
  res=read.table(paste0(setting,"_",metric,"_150_200_0.8.txt"),h=T) 
  index=c(grep("ebic",colnames(res)),grep("slope",colnames(res)),grep("jump",colnames(res)),grep("linselect",colnames(res)))
  index.bolasso=grep("_b_",colnames(res))
  index.ss=grep("_ss_",colnames(res))
  index.other=setdiff(1:ncol(res),c(index,index.bolasso,index.ss))
  
  jpeg(filename=paste0("../Images/",metric,"_",setting,"1.jpeg"),width=2250,height=2250,units="px",pointsize=36)
  par(oma=c(5,3,0,0))
  boxplot(res[,index],col=rep(c("bisque2","burlywood4","cornsilk3","darksalmon"),each=4),main="",ylim=c(0,max(1,max(res[,index]))),las=2)
  if(metric=="SUPPORT")
  {
    if (setting=="FRANK_max" | setting=="FRANK_max_shrinkage")
      abline(h=17.75,col="red",lwd=2)
    if (setting=="FRANK_min" | setting=="FRANK_min_shrinkage") 
      abline(h=1,col="red",lwd=2)
  }
  if(metric=="MSE")
    abline(h=1,col="red",lwd=3)
  if(metric=="MSE" | metric == "RECALL" | metric=="SPECIFICITY")
    abline(h=1,col="red",lwd=3)
  if (metric == "FDP")
  {
    points(apply(res[,c(index.bolasso,index.ss,index.other)],2,mean),pch=8,cex=2,lwd=3)
    abline(h=0.1,col="red",lwd=3)
    abline(h=0.05,col="red",lwd=3,lty=3) 
  }
  dev.off()
  jpeg(paste0("../Images/",metric,"_",setting,"2.jpeg"),width=2250,height=2250,pointsize=36)
  par(oma=c(5,3,0,0))
  boxplot(res[,c(index.bolasso,index.ss,index.other)],col=c(rep(c("aquamarine","brown"),each=6),rep(c("cadetblue1","chocolate"),each=2),"darkorchid"),ylim=c(0,max(1,max(res[,c(index.bolasso,index.ss,index.other)],na.rm=TRUE))),las=2)
  abline(h=1,col="red",lwd=3)
  if(metric=="SUPPORT")
  {
    if (setting=="FRANK_max" | setting=="FRANK_max_shrinkage")
      abline(h=17.75,col="red",lwd=2)
    if (setting=="FRANK_min" | setting=="FRANK_min_shrinkage") 
      abline(h=1,col="red",lwd=2)
  }
  if(metric=="MSE" | metric == "RECALL" | metric=="SPECIFICITY")
    abline(h=1,col="red",lwd=3)
  if (metric == "FDP")
  {
    points(apply(res[,c(index.bolasso,index.ss,index.other)],2,mean),pch=8,cex=2,lwd=3)
    abline(h=0.1,col="red",lwd=3)
    abline(h=0.05,col="red",lwd=3,lty=3) 
  }
  dev.off()
}
## function to generate figures as in the main text on the FRANK datasets 
representation.restricted.all=function(metric)
{
  jpeg(filename=paste0("../Images/",metric,"_FRANK.jpeg"),width=2250,height=2250,units="px",pointsize=36)
  par(mfrow=c(4,1))
  for (setting in paste0("FRANK_",c("max","max_shrinkage","min","min_shrinkage"))) 
  {
    res=read.table(paste0(setting,"_",metric,"_150_200_0.8.txt"),h=T) 
    index=grep("lars_enet",colnames(res))
    index1=match(c("lars_b_sub","lars_ss_sub","GD_escv", "GD_knockoffs","lars_tigress_lasso"),colnames(res))
    tmp=res[,c(index,index1)]
    names(tmp)=c("eBIC","slope","jump","Linselect","Bolasso","Stab. Sel.","ESCV","Knockoff","Tigress")
    boxplot(tmp,col=c("bisque2","burlywood4","cornsilk3","darksalmon","aquamarine","brown","cadetblue1","chocolate","darkorchid"),outline=FALSE,main="",ylim=c(0,max(apply(na.omit(tmp),2,quantile,0.75))),las=2,ylab=setting)
    abline(v=c(4.5),lty=3,lwd=3)
    if (metric=="MSE" | metric=="RECALL" | metric=="SPECIFICITY")
     abline(h=1,col="red",lwd=3)
    if(metric=="FDP")
    {
      points(apply(tmp,2,mean),pch=8,cex=2,lwd=3)
      abline(h=0.1,col="red",lwd=3)
      abline(h=0.05,col="red",lwd=3,lty=3) 
    }
    if(metric=="SUPPORT")
    {
      if (setting=="independent")
        abline(h=12.59,col="red",lwd=3)
      if (setting=="cluster")
        abline(h=11.63,col="red",lwd=3)
      if (setting=="scale-free_max")
        abline(h=31.41,col="red",lwd=3)
      if (setting=="scale-free_min")
        abline(h=1,col="red",lwd=3)   
    }  
  }
    dev.off()
}



### metrics for figures and annexes
setwd("Results")
################################################################################
representation("FRANK_max","MSE")
representation("FRANK_min","MSE")
representation("FRANK_max","RECALL")
representation("FRANK_min","RECALL")
representation("FRANK_max","FDP")
representation("FRANK_min","FDP")
representation("FRANK_max","SUPPORT")
representation("FRANK_min","SUPPORT")
representation("FRANK_max_shrinkage","MSE")
representation("FRANK_min_shrinkage","MSE")
representation("FRANK_max_shrinkage","RECALL")
representation("FRANK_min_shrinkage","RECALL")
representation("FRANK_max_shrinkage","FDP")
representation("FRANK_min_shrinkage","FDP")
representation("FRANK_max_shrinkage","SUPPORT")
representation("FRANK_min_shrinkage","SUPPORT")

representation.restricted.all("FDP")
representation.restricted.all("MSE")
representation.restricted.all("RECALL")
representation.restricted.all("SUPPORT")
representation.restricted.all("SPECIFICITY")
