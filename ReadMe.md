## Summary

This repository provides materials related to the paper _Comparison study of variable selection procedures in high-dimensional Gaussian linear regression_.

The goal is to compare some statistical procedures of variable selection through theirs theoretical properties and their results on simulated data. This review was motivated by biological applications, but this code can be used with other datasets.

In this repository, data are collected and simulated (parameters can be changed), statistical methods are encoded or applied from existing packages and are evaluated by all metrics used in the paper.  Number of observations as well as number of variables, parameters for the dependency structure, number of active variables for the response variable, parameters of the penalized criteria, can be easily modified since they are arguments of the functions. 

- `./DatasetsXXX/` stores the simulated datasets of size XXX and must be created before running the code. 
- `./SaveXXX/` stores the results of evaluated metrics. 
- `./Sources/` contains all the functions used in the main scripts.  

This code was developed and tested with Ubuntu 20.04 and RStudio 3.6.3. the methods were implemented using R packages mentionned at the head of the R scripts. 

## description of the different steps
### Simulation of data
First of all, data have to be simulated. We propose three scripts. The first one creates datasets of independent variables (`Simulation_independent_datasets.R`).
The second one creates datasets of correlated variables (`Simulation_scale_free_cluster_data.R`): dependencies between variables are encoded by a scale-free or a cluster network and are stored in an adjacency matrix. The third one starts with 40 sets of collected data from the [FRANK website](https://m2sb.org/) and stores data to get a matrix of expressed genes and a matrix of interactions between genes (`collect_FRANK_data.R`).  

### Parameters of the methods
They are described in the zip file `Parameters.zip`. For each scenario, we indicate the type of the simulated data, how to define the response variable, the optimization algorithm and the regularization function. A total of 24 scenarios is available.


### Application of the methods 
There are two scripts. The script `Run_Simulation_model_selection_procedure_mlm.R` is dedicated to the model selection methods.
The script `Run_Simulation_variable_identification_procedure.R` is dedicated to the variable identification methods. 

To analyze a large number of simulated datasets, we advice to use a cluster facility. 
We provide 
- a shell script `Run_Simulation_model_selection_procedure.sh` to apply the model selection methods by submitting a job (one sample in one scenario) to a  cluster computing via the qsub command.
- a shell script `Run_Simulation_model_selection_procedure.sh` to apply the model selection methods  
- a shell script `Multi_Run_Simulation_model_selection_procedure_mlm.sh` to apply the model selection methods on the 100 datasets. 


### Calculation of the metrics and creation of figures

The script `Run_Comparaison_methode_mlm.R` calculates the different metrics.
Results are stored in a folder named `Results` that has to be created before running the R script.
`Graph_150.R` uses the values of the metrics calculated on the simulated datasets of size n=150 to generate the figures of the paper, stored in a folder `Images/` that has to be created before runnning the R script. 

The folder `Creation_of_figures` contains R scripts to generate the figures of the paper and the zip file `Metrics_wrt_n.zip` contains plots about the evolution of all the methods studied in this work with respect to the sample size. Boxplots are calculated on 100 samples. The red line indicates the reference value. 
A sample size of $n=150$ is colored in light pink, $n=300$ in light blue, $n=600$ in light salmon and $n=1200$ in light steel blue.
For ESCV and the knockoffs method are based on the gradient descent algorithm. Tigress is implemented with LARS and Lasso. 

### Supplementary file 

The pdf file contains the results of all the methods considered in this simulation study. 